﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum Rating
    {
        Bad = 1,
        Normal = 2,
        Good = 3,
        Excellent = 4
    }
    public enum StatusProjectTask
    {
        //New=-1,
        Pending = 0,
        Started = 1,
        Completed = 2,
        Cancelling = 3
    }
    public enum StatusProject
    {
        New = 0,
        Started = 1,
        Completed = 2,
    }
    public enum Role
    {
        [Description("43983a6a-5c80-463e-bede-021de4bfc68f")]
        Admin = 1,

        [Description("aafbe9db-a717-49db-89ec-e2210b7e6bb8")]
        Manager = 2,

        [Description("ce3b07cb-d54f-419f-ba99-86b852b16080")]
        Member = 3,
    }
    public enum EmployeeType
    {
        [Description("Giám sát")]
        GiamSat = 1,
        [Description("Mua hàng")]
        MuaHang = 2,
        [Description("Thanh quyết toán")]
        ThanhQuyetToan =3,
        [Description("Thiết kế")]
        ThietKe=4,
        [Description("Kế toán")]
        KeToan =5
    }
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get Enum description
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value, bool resource = false)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                            Attribute.GetCustomAttribute(field,
                                typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        if (resource)
                        {
                            return "";
                        }
                        return attr.Description;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get Enum value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetValue(this Enum value)
        {
            Type type = value.GetType();

            return type.GetEnumValues().Rank;
        }
    }
}
