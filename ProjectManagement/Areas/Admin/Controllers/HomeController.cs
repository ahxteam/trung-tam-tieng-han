﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Common;
using ProjectManagement.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ProjectManagement.Areas.Admin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                //var isAdmin = User.IsInRole("Admin");

                //ViewBag.TotalUser = entities.Profiles.Count();
                //ViewBag.TotalContractor = entities.Contractors.Count();
                //ViewBag.TotalInvestor = entities.Investors.Count();
                //ViewBag.TotalProject = entities.Investors.Count();
                //// statistic jobs.
                //ViewBag.CompletedJob = entities.ProjectTasks.Count(o => o.StatusId == (int)StatusProjectTask.Completed);
                //ViewBag.StartedJob = entities.ProjectTasks.Count(o => o.StatusId == (int)StatusProjectTask.Started);
                //ViewBag.NewJob = entities.ProjectTasks.Count(o => o.StatusId == (int)StatusProjectTask.Pending);

                //// new notification
                //var currentUserId = User.Identity.GetUserId();
                //ViewBag.NewNotifications = entities.Notifications.ToList();//.Where(o => o.ReceivedUser.Equals(currentUserId) || isAdmin).OrderByDescending(o => o.CreatedDate).ToList();

                //// new status of job/task
                //ViewBag.NewTaskStatus = entities.ProjectTasks.Select(o => new ProjectTaskModel() {
                //    Name = o.Name,
                //    EndDate = o.EndDate,
                //    ProjectId = o.ProjectId,
                //    StartDate = o.StartDate,
                //    StatusId=o.StatusId,
                //    UserName = entities.Profiles.FirstOrDefault(m => m.UserId.Equals(o.UserId)).Name
                //}).ToList();//.Where(o => o.UserId.Equals(currentUserId) || isAdmin).OrderByDescending(o => o.EndDate);

                //// statistic
                //var currentDay = DateTime.Now;
                //var nextWeek = DateTime.Now.AddDays(7);//.Where(o=>o.StartDate.Value >= currentDay && o.StartDate.Value< nextWeek)
                //var statisticJob = entities.ProjectTasks.OrderBy(o => o.StartDate).ToList();
                //var data= statisticJob.GroupBy(o => new { o.StartDate.Date }, p => p, (o, p) => new
                //{
                //    y = string.Format("{0:dd/MM}", o.Date),
                //    a = p.Count()
                //}).Take(7).ToList();
                //var json = JsonConvert.SerializeObject(data);
                //ViewBag.StatisticJob = json;
                return View();
            }
            else
            {
                return Redirect("/account/login");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}