﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectManagement.Helper
{
    public class UtilHelper
    {
        public static string RejectMarks(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return String.Empty;
            }
            text = text.Trim();
            text = text.ToLower();
            string[] pattern = new string[7];
            pattern[0] = "a|(á|ả|à|ạ|ã|ă|ắ|ẳ|ằ|ặ|ẵ|â|ấ|ẩ|ầ|ậ|ẫ)";
            pattern[1] = "o|(ó|ỏ|ò|ọ|õ|ô|ố|ổ|ồ|ộ|ỗ|ơ|ớ|ở|ờ|ợ|ỡ)";
            pattern[2] = "e|(é|è|ẻ|ẹ|ẽ|ê|ế|ề|ể|ệ|ễ)";
            pattern[3] = "u|(ú|ù|ủ|ụ|ũ|ư|ứ|ừ|ử|ự|ữ)";
            pattern[4] = "i|(í|ì|ỉ|ị|ĩ)";
            pattern[5] = "y|(ý|ỳ|ỷ|ỵ|ỹ)";
            pattern[6] = "d|đ";
            for (int i = 0; i < pattern.Length; i++)
            {
                char replaceChar = pattern[i][0];
                MatchCollection matchs = Regex.Matches(text, pattern[i]);
                foreach (Match m in matchs)
                {
                    text = text.Replace(m.Value[0], replaceChar);
                }
            }
            // remove entities
            text = Regex.Replace(text, @"&\w+;", "");
            // remove anything that is not letters, numbers, dash, or space
            //text = Regex.Replace(text, @"[^a-z0-9\-\s]", "");
            // replace spaces
            text = text.Replace(":", "");
            text = text.Replace("+", "");
            text = text.Replace("/", "");
            text = text.Replace(@"\", "");
            text = text.Replace(' ', '-');
            // collapse dashes
            text = Regex.Replace(text, @"-{2,}", "-");
            // trim excessive dashes at the beginning
            text = text.TrimStart(new[] { '-' });
            // remove trailing dashes
            text = text.TrimEnd(new[] { '-' });
            return text;
        }
    }
}