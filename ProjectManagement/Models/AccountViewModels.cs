﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ProjectManagement.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage ="Bạn vui lòng nhập Email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage ="Email không hợp lệ")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Bạn vui lòng nhập mật khẩu")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage ="Bạn chưa nhập Email")]
        [EmailAddress(ErrorMessage ="Email không hợp lệ")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Bạn chưa nhập mật khẩu")]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất có {2} ký tự.", MinimumLength = 6)]
        //[DataType(DataType.Password,ErrorMessage ="Mật khẩu phải có 1 ký tự hoa, 1 ký tự đặc biệt và số. VD: Admin123@")]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Required(ErrorMessage ="Bạn chưa nhập mật khẩu xác nhận")]
        //[DataType(DataType.Password, ErrorMessage = "Mật khẩu phải có 1 ký tự hoa, 1 ký tự đặc biệt và số. VD: Admin123@")]
        [Display(Name = "Mật khẩu xác nhận")]
        [Compare("Password", ErrorMessage = "Mật khẩu không trùng khớp.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên nhà thầu")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập địa chỉ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập số điện thoại")]
        [MaxLength(50, ErrorMessage = "Số ký tự tối đa là 50")]
        public string Phone { get; set; }
        public string ProfilePicture { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
        public string UserId { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} phải ít nhất có {2} ký tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Mật khẩu không trùng khớp.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
