﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class MenuModel
    {
        public int MenuId { get; set; }
        public string Name { get; set; }
        public string UrlCode { get; set; }
        public bool Active { get; set; }
        public int OrderDisplay { get; set; }
        public string Description { get; set; }
        public int Parent { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UserId { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public Nullable<int> StatusId { get; set; }
    }
}