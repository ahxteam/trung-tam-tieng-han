﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class NewsModel
    {
        public int NewsID { get; set; }
        public string Title { get; set; }
        public string UrlCode { get; set; }
        public string ThumbPic { get; set; }
        public Nullable<int> MenuID { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
        public Nullable<int> View { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public bool Active { get; set; }
        public Nullable<int> OrderDisplay { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string Tags { get; set; }
        public HttpPostedFileBase FileBase{ get; set; }
        public string MenuName { get; set; }
        public string ActiveStr { get; set; }
    }
}