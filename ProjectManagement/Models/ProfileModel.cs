﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProfileModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên thành viên")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số điện thoại")]
        [MaxLength(50, ErrorMessage = "Số ký tự tối đa là 50")]
        public string Phone { get; set; }
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        [Required(ErrorMessage = "Bạn chưa nhập email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập địa chỉ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Address { get; set; }
        public DateTime LastLogin { get; set; }
        public string ProfilePicture { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}