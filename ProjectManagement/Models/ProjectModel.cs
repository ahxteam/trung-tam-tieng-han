﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên nhà thầu")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày bắt đầu dự án")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày kết thúc dự án")]
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int StatusID { get; set; }
        public decimal Progress { get; set; }
        public string CreatedUser { get; set; }
        public int CurrentStep { get; set; }
        public string Comment { get; set; }
        public string ManagerId { get; set; }
        public int InvestorId { get; set; }
        public string ManagerName { get; set; }
        public string InvestorName { get; set; }
    }
}