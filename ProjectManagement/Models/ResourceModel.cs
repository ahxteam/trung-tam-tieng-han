﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ResourceModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên cho tài nguyên")]
        [MaxLength(50, ErrorMessage = "Key tối đa là 50 ký tự")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập nội dung")]
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public string CreatedUser { get; set; }

        [Required(ErrorMessage ="Bạn chưa nhập key cho tài nguyên")]
        [MaxLength(50,ErrorMessage ="Key tối đa là 50 ký tự")]
        public string KeyResource { get; set; }
    }
}