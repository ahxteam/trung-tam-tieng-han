﻿var projectController = {
    currentProject:0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/project/createoredit",
            data: {
                id: projectController.currentProject
            },
            async: false
        }).done(function (response) {
            debugger;
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteProject: function () {
        $.ajax({
            method: "POST",
            url: "/project/delete",
            data: {
                id: projectController.currentProject
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    showFormView: function () {
        $.ajax({
            method: "GET",
            url: "/project/detail",
            data: {
                id: projectController.currentProject
            },
            async: false
        }).done(function (response) {
            debugger;
            $("body").append(response);
            $("#detail-btn").click();
        });
    },
    saveData: function () {
        var model = {

            Id: $("#Id").val(),
            Name: $("#Name").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            InvestorId: $("#InvestorId").val(),
            ManagerId: $("#ManagerId").val(),
            Comment: $("#Comment").val(),
            Description: $("#Description").val(),

        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/project/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);

                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    }
};