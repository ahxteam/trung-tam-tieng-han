﻿var taskController = {
    currentTask:0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/projecttask/createoredit",
            data: {
                id: taskController.currentTask
            },
            async: false
        }).done(function (response) {
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteProjectTask: function () {
        $.ajax({
            method: "POST",
            url: "/projecttask/delete",
            data: {
                id: taskController.currentTask
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
    ,
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            ProjectId: $("#ProjectId").val(),
            UserId: $("#UserId").val(),
            StartDate: $("#StartDate").val(),
            EndDate: $("#EndDate").val(),
            Description: $("#Description").val(),
        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/projecttask/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    }
};