﻿var investorController = {
    currentInvestor:0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/investor/createoredit",
            data: {
                id: investorController.currentInvestor
            },
            async: false
        }).done(function (response) {
                                   
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteInvestor: function () {
        $.ajax({
            method: "POST",
            url: "/investor/delete",
            data: {
                id: investorController.currentInvestor
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    showFormDetail: function () {
        $.ajax({
            method: "GET",
            url: "/investor/detail",
            data: {
                id: investorController.currentInvestor
            },
            async: false
        }).done(function (response) {
            $("body").append(response);
            $("#detail-btn").click();
        });
    },
        saveData:function(){
            var model = {
                Id: $("#Id").val(),
                Name: $("#Name").val(),
                Email: $("#Email").val(),
                Phone: $("#Phone").val(),
                Address: $("#Address").val(),
                Note: $("#Note").val(),
                Rating: $("input[name=Rating]:checked").val(),
            }
            var vld = window.baseJs.validation;
            var $form = $("#create");
            var jsonData = JSON.stringify(model);
            $.ajax({
                url: '/investor/createoredit',
                data: jsonData,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                cache: false,
                success: function (response) {
                    if (response.result === true) {
                        $('#con-close-modal').modal('toggle');
                        window.location.reload();
                    } else if (response.Errors) {
                        vld.clear($form);
                        vld.all(response.Errors, $form);
                    
                    } else {
                        $('#con-close-modal').modal('toggle');
                    }
                }
            });
        }
};