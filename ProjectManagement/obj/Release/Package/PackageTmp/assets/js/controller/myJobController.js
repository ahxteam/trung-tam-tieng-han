﻿var myJobController = {
    currentMyJob:0,
    showFormUpdate: function () {
        $.ajax({
            method: "GET",
            url: "/myjob/updatejob",
            data: {
                id: myJobController.currentMyJob
            },
            async: false
        }).done(function (response) {
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=show-detail]").click();
        });
    }
};