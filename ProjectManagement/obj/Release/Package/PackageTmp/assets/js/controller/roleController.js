﻿var roleControler = {
    currentRole: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/member/createoreditrole",
            data: {
                id: roleControler.currentRole
            },
            async: false
        }).done(function (response) {
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deletePosition: function () {
        $.ajax({
            method: "POST",
            url: "/member/deleterole",
            data: {
                id: roleControler.currentRole
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData:function(){
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Description: $("#Description").val(),
            RoleId: $("#RoleId").val(),
        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/member/createoreditrole',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                debugger;
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                    
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    }
    //dialogConfirm
};