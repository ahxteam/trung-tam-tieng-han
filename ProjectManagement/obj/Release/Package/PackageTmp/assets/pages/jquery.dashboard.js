﻿
/**
* Theme: Adminto Admin Template
* Author: Coderthemes
* Dashboard
*/

!function ($) {
    "use strict";

    var Dashboard1 = function () {
        this.$realData = []
    };

    //creates Bar chart
    Dashboard1.prototype.createBarChart = function (element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barSizeRatio: 0.2,
            barColors: lineColors
        });
    },

    //creates line chart
    Dashboard1.prototype.createLineChart = function (element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Line({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            fillOpacity: opacity,
            pointFillColors: Pfillcolor,
            pointStrokeColors: Pstockcolor,
            behaveLikeLine: true,
            gridLineColor: '#eef0f2',
            hideHover: 'auto',
            resize: true, //defaulted to true
            pointSize: 0,
            lineColors: lineColors
        });
    },

    //creates Donut chart
    Dashboard1.prototype.createDonutChart = function (element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors
        });
    },


    Dashboard1.prototype.init = function (barData, donutData) {

        //creating bar chart
        //var $barData = [
        //    { y: '29/09', a: 75 },
        //    { y: '30/09', a: 42 },
        //    { y: '01/10', a: 75 },
        //    { y: '02/10', a: 38 },
        //    { y: '03/10', a: 19 },
        //    { y: '04/10', a: 93 }
        //];
        var $barData = barData;
        this.createBarChart('morris-bar-example', $barData, 'y', ['a'], ['Statistics'], ['#188ae2']);

        //creating donut chart
        //var $donutData = [
        //        { label: "", value: 2 },
        //        { label: "", value: 28 },
        //        { label: "", value: 16 }
        //];
        var $donutData = donutData;
        this.createDonutChart('morris-donut-example', $donutData, ['#ff8acc', '#5b69bc', "#35b8e0"]);
    },
    //init
    $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery);

